package model;

public class Produs{
	private int id;
	private String nume;
	private int cantitate;
	private int pret;

	public Produs() {
	}

	public Produs(int id, String nume, int cantitate, int pret) {
		super();
		this.id = id;
		this.nume = nume;
		this.cantitate = cantitate;
		this.pret = pret;
	}

	public Produs(String nume, int cantitate, int pret) {
		super();
		this.nume = nume;
		this.cantitate = cantitate;
		this.pret = pret;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	
	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}
}