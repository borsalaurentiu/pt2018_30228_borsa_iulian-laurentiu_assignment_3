package model;

public class Comanda{
	private int id;
	private int client;
	private String produs;
	private int cantitate;
	private int total;

	public Comanda() {
	}

	public Comanda(int id, int client, String produs, int cantitate, int total) {
		super();
		this.id = id;
		this.client = client;
		this.produs = produs;
		this.cantitate = cantitate;
		this.total = total;
	}

	public Comanda(int client, String produs, int cantitate, int total) {
		super();
		this.client = client;
		this.produs = produs;
		this.cantitate = cantitate;
		this.total = total;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getClient() {
		return client;
	}

	public void setClient(int client) {
		this.client = client;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	

	public String getProdus() {
		return produs;
	}

	public void setProdus(String produs) {
		this.produs = produs;
	}
}