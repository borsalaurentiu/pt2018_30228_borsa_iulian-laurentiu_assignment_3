package model;

public class Bon{
	private int id;
	private int comanda;
	private int total;
	private String eliberat;
	
	public Bon() {
	}

	public Bon(int id, int comanda, int total, String eliberat) {
		super();
		this.id = id;
		this.comanda = comanda;
		this.total = total;
		this.eliberat = eliberat;
	}

	public Bon(int comanda, int total, String eliberat) {
		super();
		this.comanda = comanda;
		this.total = total;
		this.eliberat = eliberat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getComanda() {
		return comanda;
	}

	public void setComanda(int comanda) {
		this.comanda = comanda;
	}	
	public String getEliberat() {
		return eliberat;
	}

	public void setEliberat(String eliberat) {
		this.eliberat = eliberat;
	}	
}