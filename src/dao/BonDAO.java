package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Bon;
import model.Comanda;

public class BonDAO {

	protected static final Logger LOGGER = Logger.getLogger(BonDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO bon (comanda, total, eliberat)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM bon where id = ?";
	private final static String getStatementString = "SELECT * FROM bon";
	private final static String elibereazaStatementString = "UPDATE bon SET eliberat = ? where id = ?";
	
	public static Bon findById(int id) {
		Bon toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			rs.next();

			int comanda = rs.getInt("comanda");
			int total = rs.getInt("total");
			String eliberat = rs.getString("eliberat");
			toReturn = new Bon(id, comanda, total, eliberat);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"BonDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int insert(Bon bon) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, bon.getComanda());
			insertStatement.setInt(2, bon.getTotal());
			insertStatement.setString(3, "nu");
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "BonDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}


	public static String elibereaza(int id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement updateStatement = null;
		String updatedId = "da";
		try {
			updateStatement = dbConnection.prepareStatement(elibereazaStatementString);
			updateStatement.setString(1, updatedId);
			updateStatement.setInt(2, id);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "BonDAO:elibereaza " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}

	
	public static ArrayList<Bon> getBonuri() throws SQLException{
		ArrayList<Bon> bonuri = new ArrayList<Bon>();
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement getStatement = null;
		try {
			getStatement = dbConnection.prepareStatement(getStatementString);
			ResultSet rs = getStatement.executeQuery(getStatementString);
			while(rs.next()) {
				Bon bon = new Bon(Integer.parseInt(rs.getString("id")),Integer.parseInt(rs.getString("comanda")),Integer.parseInt(rs.getString("total")),rs.getString("eliberat"));
				bonuri.add(bon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "BonDAO:getBonuri " + e.getMessage());
		} finally {
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(dbConnection);
		}
		return bonuri;
	}
}
