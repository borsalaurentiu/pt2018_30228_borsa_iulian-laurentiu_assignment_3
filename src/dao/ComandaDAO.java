package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Comanda;

public class ComandaDAO {

	protected static final Logger LOGGER = Logger.getLogger(ComandaDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO comanda (client,produs,cantitate,total)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM comanda where id = ?";
	private final static String getStatementString = "SELECT * FROM comanda";	
	
	public static Comanda findById(int id) {
		Comanda toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			
			int client = rs.getInt("client");
			String produs = rs.getString("produs");
			int cantitate = rs.getInt("cantitate");
			int total = rs.getInt("total");
			toReturn = new Comanda(id, client, produs, cantitate, total);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ComandaDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Comanda comanda) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, comanda.getClient());
			insertStatement.setString(2, comanda.getProdus());
			insertStatement.setInt(3, comanda.getCantitate());
			insertStatement.setInt(4, comanda.getTotal());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static ArrayList<Comanda> getComenzi() throws SQLException{
		ArrayList<Comanda> comenzi = new ArrayList<Comanda>();
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement getStatement = null;
		try {
			getStatement = dbConnection.prepareStatement(getStatementString);
			ResultSet rs = getStatement.executeQuery(getStatementString);
			while(rs.next()) {
				Comanda comanda = new Comanda(Integer.parseInt(rs.getString("id")),Integer.parseInt(rs.getString("client")),rs.getString("produs"),Integer.parseInt(rs.getString("cantitate")),Integer.parseInt(rs.getString("total")));
				comenzi.add(comanda);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ComandaDAO:getComenzi " + e.getMessage());
		} finally {
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(dbConnection);
		}
		return comenzi;
	}
}
