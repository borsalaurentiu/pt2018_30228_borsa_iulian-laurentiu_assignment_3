package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (nume,adresa,telefon,email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM client where id = ?";
	private final static String deleteStatementString = "DELETE FROM client where id = ?";
	private final static String updateStatementString = "UPDATE client SET nume=?, adresa=?, telefon=?, email=? where id = ?";
	private final static String getStatementString = "SELECT * FROM client";


	public static Client findById(int id) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			rs.next();

			String nume = rs.getString("nume");
			String adresa = rs.getString("adresa");
			String telefon = rs.getString("telefon");
			String email = rs.getString("email");
			toReturn = new Client(id, nume, adresa, telefon, email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getNume());
			insertStatement.setString(2, client.getAdresa());
			insertStatement.setString(3, client.getTelefon());
			insertStatement.setString(4, client.getEmail());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static int update(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int updatedId = client.getId();
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(5, updatedId);
			updateStatement.setString(1, client.getNume());
			updateStatement.setString(2, client.getAdresa());
			updateStatement.setString(3, client.getTelefon());
			updateStatement.setString(4, client.getEmail());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}


	public static int delete(int client) {
		Connection dbConnection = ConnectionFactory.getConnection();
	
		PreparedStatement deleteStatement = null;
		int deletedId = client;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, deletedId);
			deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return deletedId;
	}

	public static ArrayList<Client> getClienti() throws SQLException{
		ArrayList<Client> clienti = new ArrayList<Client>();
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement getStatement = null;
		try {
			getStatement = dbConnection.prepareStatement(getStatementString);
			ResultSet rs = getStatement.executeQuery(getStatementString);
			while(rs.next()) {
				Client client = new Client(Integer.parseInt(rs.getString("id")),rs.getString("nume"),rs.getString("adresa"),rs.getString("telefon"),rs.getString("email"));
				clienti.add(client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:getClienti " + e.getMessage());
		} finally {
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(dbConnection);
		}
		return clienti;
	}
}
