package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Produs;

public class ProdusDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProdusDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO produs (nume,cantitate,pret)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM produs where id = ?";
	private final static String deleteStatementString = "DELETE FROM produs where id = ?";
	private final static String updateStatementString = "UPDATE produs SET nume=?, cantitate=?, pret=? where id = ?";
	private final static String getStatementString = "SELECT * FROM produs";	
	private final static String pretStatementString = "SELECT pret FROM produs where nume = ?";
	private final static String cantitateStatementString = "SELECT cantitate FROM produs where nume = ?";
	private final static String updateCantitateStatementString = "UPDATE produs SET cantitate = cantitate - ? where nume = ?";
	
	public static Produs findById(int id) {
		Produs toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, id);
			rs = findStatement.executeQuery();
			rs.next();
			
			String nume = rs.getString("nume");
			int cantitate = rs.getInt("cantitate");
			int pret = rs.getInt("pret");
			toReturn = new Produs(id, nume, cantitate, pret);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int findPret(String nume) {
		int toReturn = 0;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(pretStatementString);
			findStatement.setString(1, nume);
			rs = findStatement.executeQuery();
			rs.next();
			
			int pret = rs.getInt("pret");
			
			toReturn = pret;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findPret " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}


	public static int findCantitate(String nume) {
		int toReturn = 0;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(cantitateStatementString);
			findStatement.setString(1, nume);
			rs = findStatement.executeQuery();
			rs.next();
			
			int cantitate = rs.getInt("cantitate");
			
			toReturn = cantitate;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProdusDAO:findCantitate" + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	
	public static int insert(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, produs.getNume());
			insertStatement.setInt(2, produs.getCantitate());
			insertStatement.setInt(3, produs.getPret());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static int update(Produs produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement updateStatement = null;
		int updatedId = produs.getId();
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(4, updatedId);
			updateStatement.setString(1, produs.getNume());
			updateStatement.setInt(2, produs.getCantitate());
			updateStatement.setInt(3, produs.getPret());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}


	public static String updateCantitate(String nume, int cantitateCeruta) {
		Connection dbConnection = ConnectionFactory.getConnection();
		
		PreparedStatement updateStatement = null;
		String updatedId = nume;
		try {
			updateStatement = dbConnection.prepareStatement(updateCantitateStatementString);
			updateStatement.setString(2, updatedId);
			updateStatement.setInt(1, cantitateCeruta);
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:updateCantitate " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
		return updatedId;
	}


	public static int delete(int produs) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement deleteStatement = null;
		int deletedId = produs;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, deletedId);
			deleteStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		return deletedId;
	}

	public static ArrayList<Produs> getProduse() throws SQLException{
		ArrayList<Produs> produse = new ArrayList<Produs>();
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement getStatement = null;
		try {
			getStatement = dbConnection.prepareStatement(getStatementString);
			ResultSet rs = getStatement.executeQuery(getStatementString);
			while(rs.next()) {
				Produs produs = new Produs(Integer.parseInt(rs.getString("id")),rs.getString("nume"),Integer.parseInt(rs.getString("cantitate")),Integer.parseInt(rs.getString("pret")));
				produse.add(produs);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProdusDAO:getProduse " + e.getMessage());
		} finally {
			ConnectionFactory.close(getStatement);
			ConnectionFactory.close(dbConnection);
		}
		return produse;
	}
}
