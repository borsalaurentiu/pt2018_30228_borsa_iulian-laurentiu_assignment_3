package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import bll.ProdusBLL;
import dao.ProdusDAO;
import model.Produs;

import java.sql.SQLException;

public class ProduseGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JTable tabel = new JTable();

	private JTextField stergeIdText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField numeText = new JTextField();
	private JTextField cantitateText = new JTextField();
	private JTextField pretText = new JTextField();
	private JTextField reportText = new JTextField();

	private JLabel idLabel = new JLabel("Update dupa ID:");
	private JLabel numeLabel = new JLabel("Nume:");
	private JLabel cantitateLabel = new JLabel("Cantitate:");
	private JLabel pretLabel = new JLabel("Pret/unitate:");
	private JLabel stergeLabel = new JLabel("Se va sterge produsul dupa ID.");

	private JButton adaugaButton = new JButton("Adauga");
	private JButton stergeButton = new JButton("Sterge");
	private JButton editeazaButton = new JButton("Editeaza");

	public ProduseGUI() throws SQLException {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		adaugareAscultatori();
		adaugareTabel();
	}

	public void initializareEcran() {
		setTitle("Tema 3 - Produse");
		setSize(new Dimension(750, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		add(panel);
	}

	public void initializareComponente() {

		stergeIdText.setBounds(125, 335, 90, 30);
		idText.setBounds(125, 50, 90, 30);
		numeText.setBounds(125, 100, 90, 30);
		cantitateText.setBounds(125, 150, 90, 30);
		pretText.setBounds(125, 200, 90, 30);
		reportText.setBounds(240, 335, 480, 30);		
		reportText.setEditable(false);

		idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		cantitateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pretLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		stergeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stergeIdText.setHorizontalAlignment(SwingConstants.CENTER);
		reportText.setHorizontalAlignment(SwingConstants.CENTER);

		idLabel.setBounds(25, 50, 90, 30);
		numeLabel.setBounds(25, 100, 90, 30);
		cantitateLabel.setBounds(25, 150, 90, 30);
		pretLabel.setBounds(25, 200, 90, 30);
		stergeLabel.setBounds(25, 290, 190, 30);

		adaugaButton.setBounds(25, 245, 90, 30);
		editeazaButton.setBounds(125, 245, 90, 30);
		stergeButton.setBounds(25, 335, 90, 30);

		tabel.setBounds(240, 50, 480, 260);


	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(adaugaButton);
		panel.add(stergeButton);
		panel.add(editeazaButton);
		panel.add(stergeIdText);
		panel.add(idText);
		panel.add(numeText);
		panel.add(cantitateText);
		panel.add(pretText);
		panel.add(idLabel);
		panel.add(numeLabel);
		panel.add(cantitateLabel);
		panel.add(pretLabel);
		panel.add(stergeLabel);
		panel.add(reportText);
		panel.add(tabel);
	}

	public void adaugareTabel() throws SQLException  {
		ProdusBLL produsBll = new ProdusBLL();
		ArrayList<Produs> produse = new ArrayList<Produs>();
		produse = produsBll.getProduse();

		String[] coloane = {"ID","Nume","Cantitate","Pret/unitate"};		
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("id");
		model.addColumn("nume");
		model.addColumn("cantitate");
		model.addColumn("pret");
		model.addRow(coloane);	

		for(Produs produs: produse){
			Object[] row = new Object[4];
			row[0] = produs.getId();
			row[1] = produs.getNume();
			row[2] = produs.getCantitate(); 
			row[3] = produs.getPret();
			model.addRow(row);
		}
		tabel.setModel(model);
	}


	public void adaugareAscultatori() {
		adaugaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produs produs = new Produs();
				produs.setNume(numeText.getText());
				produs.setCantitate(Integer.parseInt(cantitateText.getText()));
				produs.setPret(Integer.parseInt(pretText.getText()));
				ProdusDAO.insert(produs);
				String report = "Produsul a fost adaugat cu succes!";
				reportText.setText(report);
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		editeazaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Produs produs = new Produs();
				produs.setId(Integer.parseInt(idText.getText()));
				produs.setNume(numeText.getText());
				produs.setCantitate(Integer.parseInt(cantitateText.getText()));
				produs.setPret(Integer.parseInt(pretText.getText()));
				ProdusDAO.update(produs);
				String report = "Produsul " + produs.getId() + " a fost editat cu succes!";
				reportText.setText(report);
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		stergeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int produsId = Integer.parseInt(stergeIdText.getText());
				ProdusDAO.delete(produsId);
				String report = new String();
				report = "Produsul " + produsId + " a fost sters cu succes!";
				reportText.setText(report);
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) throws SQLException {
		new ProduseGUI();
	}
}
