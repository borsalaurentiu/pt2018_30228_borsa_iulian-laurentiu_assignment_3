package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import bll.ComandaBLL;
import bll.BonBLL;
import dao.ComandaDAO;
import dao.ProdusDAO;
import dao.BonDAO;
import dao.ClientDAO;
import model.Comanda;
import model.Bon;
import model.Client;

import java.sql.SQLException;

public class ComenziGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JTable tabelComenzi = new JTable();
	private JTable tabelBonuri = new JTable();

	private JTextField elibereazaIdText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField numeText = new JTextField();
	private JTextField cantitateText = new JTextField();
	private JTextField reportText = new JTextField();

	private JLabel idLabel = new JLabel("Client ID:");
	private JLabel numeLabel = new JLabel("Nume produs:");
	private JLabel cantitateLabel = new JLabel("Cantitate:");
	private JLabel elibereazaLabel = new JLabel("Se va elibera bonul dupa ID.");

	private JButton comandaButton = new JButton("Comanda");
	private JButton elibereazaButton = new JButton("Elibereaza bon");

	public ComenziGUI() throws SQLException {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		adaugareAscultatori();
		adaugareComenziTabel();
		adaugareBonuriTabel();
	}

	public void initializareEcran() {
		setTitle("Tema 3 - Comenzi si bonuri");
		setSize(new Dimension(750, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		add(panel);
	}

	public void initializareComponente() {

		elibereazaIdText.setBounds(25, 365, 190, 30);
		idText.setBounds(125, 25, 90, 30);
		numeText.setBounds(125, 75, 90, 30);
		cantitateText.setBounds(125, 125, 90, 30);
		reportText.setBounds(25, 215, 695, 35);		
		reportText.setEditable(false);

		idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		cantitateLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		elibereazaLabel.setHorizontalAlignment(SwingConstants.CENTER);
		elibereazaIdText.setHorizontalAlignment(SwingConstants.CENTER);
		reportText.setHorizontalAlignment(SwingConstants.CENTER);

		idLabel.setBounds(25, 25, 90, 30);
		numeLabel.setBounds(25, 75, 90, 30);
		cantitateLabel.setBounds(25, 125, 90, 30);
		elibereazaLabel.setBounds(25, 315, 190, 30);

		comandaButton.setBounds(25, 170, 190, 30);
		elibereazaButton.setBounds(25, 265, 190, 30);

		tabelComenzi.setBounds(240, 25, 480, 175);
		tabelBonuri.setBounds(240, 265, 480, 130);


	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(comandaButton);
		panel.add(elibereazaButton);
		panel.add(elibereazaIdText);
		panel.add(idText);
		panel.add(numeText);
		panel.add(cantitateText);
		panel.add(idLabel);
		panel.add(numeLabel);
		panel.add(cantitateLabel);
		panel.add(elibereazaLabel);
		panel.add(reportText);
		panel.add(tabelComenzi);
		panel.add(tabelBonuri);
	}

	public void adaugareComenziTabel() throws SQLException  {
		ComandaBLL comandaBll = new ComandaBLL();
		ArrayList<Comanda> comenzi = new ArrayList<Comanda>();
		comenzi = comandaBll.getComenzi();

		String[] coloane = {"ID","ID Client","Produs","Cantitate","Total"};		
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("id");
		model.addColumn("client");
		model.addColumn("produs");
		model.addColumn("cantitate");
		model.addColumn("total");
		model.addRow(coloane);	

		for(Comanda comanda: comenzi){
			Object[] row = new Object[5];
			row[0] = comanda.getId();
			row[1] = comanda.getClient();
			row[2] = comanda.getProdus(); 
			row[3] = comanda.getCantitate();
			row[4] = comanda.getTotal();
			model.addRow(row);
		}
		tabelComenzi.setModel(model);
	}

	public void adaugareBonuriTabel() throws SQLException  {
		BonBLL bonBll = new BonBLL();
		ArrayList<Bon> bonuri = new ArrayList<Bon>();
		bonuri = bonBll.getBonuri();

		String[] coloane = {"ID","ID Comanda","Total","Eliberat"};		
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("id");
		model.addColumn("comanda");
		model.addColumn("total");
		model.addColumn("eliberat");
		model.addRow(coloane);	

		for(Bon bon: bonuri){
			Object[] row = new Object[4];
			row[0] = bon.getId();
			row[1] = bon.getComanda();
			row[2] = bon.getTotal(); 
			row[3] = bon.getEliberat();
			model.addRow(row);
		}
		tabelBonuri.setModel(model);
	}

	public void elibereaza(Bon bon) {
		String file = new String();
		file = "bon"+bon.getId()+".txt";
		String text = new String();

		Comanda comanda = new Comanda();
		comanda = ComandaDAO.findById(bon.getId());

		Client client = new Client();
		client = ClientDAO.findById(comanda.getClient());

		try {
			PrintWriter fisier = new PrintWriter(file, "UTF-8");
			text = "Bonul no_" + bon.getId();
			fisier.println(text);
			text = client.toString(); 
			fisier.println(text);
			text = "A comandat " + comanda.getCantitate() + "x " + comanda.getProdus();
			fisier.println(text);
			text = "Total plata: " + bon.getTotal() + " lei.";
			fisier.println(text);
			fisier.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void adaugareAscultatori() {
		comandaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda comanda = new Comanda();
				comanda.setClient(Integer.parseInt(idText.getText()));
				comanda.setProdus(numeText.getText());
				comanda.setCantitate(Integer.parseInt(cantitateText.getText()));
				int totalPret = 0;
				int cantitate = 0;
				totalPret = ProdusDAO.findPret(comanda.getProdus())*comanda.getCantitate();
				cantitate = ProdusDAO.findCantitate(comanda.getProdus());
				if(cantitate >= comanda.getCantitate()) {
					comanda.setTotal(totalPret);
					int o = ComandaDAO.insert(comanda);
					ProdusDAO.updateCantitate(comanda.getProdus(), comanda.getCantitate());
					Bon bon = new Bon(o, comanda.getTotal(),"nu");
					BonDAO.insert(bon);


					String report = "Comanda a fost adaugata cu succes!";
					reportText.setText(report);
					try {
						adaugareComenziTabel();
						adaugareBonuriTabel();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else
				{
					String report = "Nu exista suficiente produse in stoc!";
					reportText.setText(report);
				}
			}
		});

		elibereazaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id;
				id = Integer.parseInt(elibereazaIdText.getText());
				BonDAO.elibereaza(id);
				try {
					adaugareBonuriTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				Bon bon = BonDAO.findById(id);
				elibereaza(bon);
				String report = "Bonul a fost eliberat cu succes!";
				reportText.setText(report);

			}
		});
	}

	public static void main(String[] args) throws SQLException {
		new ComenziGUI();
	}
}
