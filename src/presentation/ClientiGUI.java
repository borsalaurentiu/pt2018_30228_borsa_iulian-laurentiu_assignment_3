package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import bll.ClientBLL;
import dao.ClientDAO;
import model.Client;

import java.sql.SQLException;

public class ClientiGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JTable tabel = new JTable();

	private JTextField stergeIdText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField numeText = new JTextField();
	private JTextField adresaText = new JTextField();
	private JTextField telefonText = new JTextField();
	private JTextField emailText = new JTextField();
	private JTextField reportText = new JTextField();

	private JLabel idLabel = new JLabel("Update dupa ID:");
	private JLabel numeLabel = new JLabel("Nume:");
	private JLabel adresaLabel = new JLabel("Adresa:");
	private JLabel telefonLabel = new JLabel("Telefon:");
	private JLabel emailLabel = new JLabel("Email:");
	private JLabel stergeLabel = new JLabel("Se va sterge clientul dupa ID.");

	private JButton adaugaButton = new JButton("Adauga");
	private JButton stergeButton = new JButton("Sterge");
	private JButton editeazaButton = new JButton("Editeaza");

	public ClientiGUI() throws SQLException {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		adaugareAscultatori();
		adaugareTabel();
	}

	public void initializareEcran() {
		setTitle("Tema 3 - Clienti");
		setSize(new Dimension(750, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		add(panel);
	}

	public void initializareComponente() {

		stergeIdText.setBounds(125, 360, 90, 30);
		idText.setBounds(125, 25, 90, 30);
		numeText.setBounds(125, 75, 90, 30);
		adresaText.setBounds(125, 125, 90, 30);
		telefonText.setBounds(125, 175, 90, 30);
		emailText.setBounds(125, 225, 90, 30);		
		reportText.setBounds(240, 360, 480, 30);		
		reportText.setEditable(false);

		idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		adresaLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		telefonLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		emailLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		stergeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stergeIdText.setHorizontalAlignment(SwingConstants.CENTER);
		reportText.setHorizontalAlignment(SwingConstants.CENTER);

		idLabel.setBounds(25, 25, 90, 30);
		numeLabel.setBounds(25, 75, 90, 30);
		adresaLabel.setBounds(25, 125, 90, 30);
		telefonLabel.setBounds(25, 175, 90, 30);
		emailLabel.setBounds(25, 225, 90, 30);
		stergeLabel.setBounds(25, 315, 190, 30);

		adaugaButton.setBounds(25, 270, 90, 30);
		editeazaButton.setBounds(125, 270, 90, 30);
		stergeButton.setBounds(25, 360, 90, 30);

		tabel.setBounds(240, 25, 480, 310);


	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(adaugaButton);
		panel.add(stergeButton);
		panel.add(editeazaButton);
		panel.add(stergeIdText);
		panel.add(idText);
		panel.add(numeText);
		panel.add(adresaText);
		panel.add(telefonText);
		panel.add(emailText);
		panel.add(idLabel);
		panel.add(numeLabel);
		panel.add(adresaLabel);
		panel.add(telefonLabel);
		panel.add(emailLabel);
		panel.add(stergeLabel);
		panel.add(reportText);
		panel.add(tabel);
	}

	public void adaugareTabel() throws SQLException  {
		ClientBLL clientBll = new ClientBLL();
		ArrayList<Client> clienti = new ArrayList<Client>();
		clienti = clientBll.getClienti();

		String[] coloane = {"ID","Nume","Adresa","Telefon","Email"};		
		DefaultTableModel model = new DefaultTableModel();
		
		model.addColumn("id");
		model.addColumn("nume");
		model.addColumn("adresa");
		model.addColumn("telefon");
		model.addColumn("email");
		model.addRow(coloane);	
		
		for(Client client: clienti){
			Object[] row = new Object[5];
			row[0] = client.getId();
			row[1] = client.getNume();
			row[2] = client.getAdresa(); 
			row[3] = client.getTelefon(); 
			row[4] = client.getEmail();
			model.addRow(row);
		}
		tabel.setModel(model);
	}

	
	public void adaugareAscultatori() {
		adaugaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client client = new Client();
				//client.setId(Integer.parseInt(idText.getText()));
				client.setNume(numeText.getText());
				client.setAdresa(adresaText.getText());
				client.setTelefon(telefonText.getText());
				client.setEmail(emailText.getText());
				ClientDAO.insert(client);
				String report = "Clientul a fost adaugat cu succes!";
				reportText.setText(report);
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		editeazaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client client = new Client();
				client.setId(Integer.parseInt(idText.getText()));
				client.setNume(numeText.getText());
				client.setAdresa(adresaText.getText());
				client.setTelefon(telefonText.getText());
				client.setEmail(emailText.getText());
				ClientDAO.update(client);
				String report = "Clientul " + client.getId() + " a fost editat cu succes!";
				reportText.setText(report);					
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		stergeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int clientId = Integer.parseInt(stergeIdText.getText());
				ClientDAO.delete(clientId);
				String report = new String();
				report = "Clientul " + clientId + " a fost sters cu succes!";
				reportText.setText(report);
				try {
					adaugareTabel();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) throws SQLException {
		new ClientiGUI();
	}
}
