package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.sql.SQLException;

public class DepozitGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JButton clientiButton = new JButton("Clienti");
	private JButton comenziButton = new JButton("Comenzi si bonuri");
	private JButton produseButton = new JButton("Produse");

	public DepozitGUI() {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		adaugareAscultatori();
	}

	public void initializareEcran() {
		setTitle("Tema 3 - Depozit");
		setSize(new Dimension(750, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
	}

	public void initializareComponente() {
		clientiButton.setBounds(100, 50, 150, 50);
		produseButton.setBounds(300, 50, 150, 50);
		comenziButton.setBounds(500, 50, 150, 50);
	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(clientiButton);
		panel.add(produseButton);
		panel.add(comenziButton);
	}

	public void adaugareAscultatori() {

		clientiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientiGUI clientiGUI;
				try {
					clientiGUI = new ClientiGUI();
					clientiGUI.setVisible(true);
				}catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		comenziButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComenziGUI comenziGUI;
				try {
					comenziGUI = new ComenziGUI();
					comenziGUI.setVisible(true);
				}catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});

		produseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProduseGUI produseGUI;
				try {
					produseGUI = new ProduseGUI();
					produseGUI.setVisible(true);
				}catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	public static void main(String[] args) {
		new DepozitGUI();
	}
}
