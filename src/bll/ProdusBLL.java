package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import dao.ProdusDAO;
import model.Produs;

public class ProdusBLL {

	private ProdusDAO produsDAO;
	
	public ProdusBLL() {
		produsDAO = new ProdusDAO();
	}

	public ArrayList<Produs> getProduse() throws SQLException{
		ArrayList<Produs> produse = new ArrayList<Produs>();
		produse = ProdusDAO.getProduse();
		return produse;
	}
}
