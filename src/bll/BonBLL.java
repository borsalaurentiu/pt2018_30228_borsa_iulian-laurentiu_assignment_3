package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import dao.BonDAO;
import model.Bon;

public class BonBLL {

	private BonDAO bonDAO;

	public BonBLL() {		
		bonDAO = new BonDAO();
	}
	
	public ArrayList<Bon> getBonuri() throws SQLException{
		ArrayList<Bon> bonuri = new ArrayList<Bon>();
		bonuri = BonDAO.getBonuri();
		return bonuri;
	}
}
