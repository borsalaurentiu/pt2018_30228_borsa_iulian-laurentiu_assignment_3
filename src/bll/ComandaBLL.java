package bll;

import java.sql.SQLException;
import java.util.ArrayList;

import dao.ComandaDAO;
import model.Comanda;

public class ComandaBLL {

	private ComandaDAO comandaDAO;

	public ComandaBLL() {	
		comandaDAO = new ComandaDAO();
	}
	
	public ArrayList<Comanda> getComenzi() throws SQLException{
		ArrayList<Comanda> comenzi = new ArrayList<Comanda>();
		comenzi = ComandaDAO.getComenzi();
		return comenzi;
	}
}
