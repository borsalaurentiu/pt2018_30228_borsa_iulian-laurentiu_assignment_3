package bll;

import java.sql.SQLException;
import java.util.ArrayList;
import dao.ClientDAO;
import model.Client;

public class ClientBLL {

	private ClientDAO ClientDAO;

	public ClientBLL() {	
		ClientDAO = new ClientDAO();
	}

	public ArrayList<Client> getClienti() throws SQLException{
		ArrayList<Client> clienti = new ArrayList<Client>();
		clienti = ClientDAO.getClienti();
		return clienti;
	}
}
